import { offerService } from '../_services';
import { router } from '../_routers';

const state = {
    all: {},
    single: {}
};

const actions = {
    getAll({ dispatch, commit }) {
        commit('getAllOffersRequest');

        offerService.getAllOffers()
            .then(
                data => {
                    commit('getAllOffersSuccess', data);
                    //router.push('/');
                },
                error => {
                    commit('getAllOffersFailure', error);
                    dispatch('alert/error', error, { root: true });
                }
            );
    },
    getSingle({dispatch, commit}, id){
        commit('getSingleOfferRequest', id);

        offerService.getSingleOffer(id)
            .then(
                data => {
                    commit('getSingleOfferSuccess', {id, data});
                },
                error => {
                    commit('getSingleOfferFailure', {id, error});
                    dispatch('alert/error', error, { root: true });
                }
            );
    },
    createSingleOffer({dispatch, commit}, params){
        commit('createSingleOfferRequest', params);

        offerService.createSingle(params)
            .then(
                data => {
                    const { id } = data;
                    commit('createSingleOfferSuccess', {id, data});
                    router.push('/');
                },
                error => {
                    console.log(error);
                    commit('createSingleOfferFailure', error);
                    dispatch('alert/error', error, { root: true });
                }
            );
    }
};

const mutations = {

    getAllOffersRequest(state){
        state.all = {loading: true };
    },
    getAllOffersSuccess(state, offers){
        state.all =  offers.results ;
    },
    getAllOffersFailure(state, error){
        state.all = { error };
    },
    getSingleOfferRequest(state, offer){
        state[offer.id] = null;
        state.single = state[offer.id];
    },
    getSingleOfferSuccess(state, offer){
        state[offer.id] = offer.data;
        state.single = state[offer.id];
    },
    getSingleOfferFailure(state, offer){
        state[offer.id] = offer.error;
    },
    createSingleOfferRequest(state){
        state.all = { loading: true };
    },
    createSingleOfferSuccess(state, offer){
        state[offer.id] = offer.data;
        state.single = state[offer.id];
    },
    createSingleOfferFailure(state, error){
        state.all = { error };
    },

};

export const offers = {
    namespaced: true,
    state,
    actions,
    mutations
};

