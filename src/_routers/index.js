import Vue from 'vue';
import Router from 'vue-router';

import OffersPage from '../pages/OffersPage';
import LoginPage from '../pages/LoginPage';
import OfferCreate from '../pages/OfferCreate';
import OfferPage from '../pages/OfferPage';
import RegisterPage from '../pages/RegisterPage';

Vue.use(Router);

export const router = new Router({
    mode: 'history',
    routes: [
        { path: '/', component: OffersPage },
        { path: '/login', component: LoginPage },
        { path: '/register', component: RegisterPage },
        { path: '/create', component: OfferCreate },
        { path: '/offer/:id', component: OfferPage },

        // otherwise redirect to home
        { path: '*', redirect: '/' }
    ]
});

router.beforeEach((to, from, next) => {
    // redirect to login page if not logged in and trying to access a restricted page
    const publicPages = ['/login', '/register'];
    const authRequired = !publicPages.includes(to.path);
    const loggedIn = localStorage.getItem('user');

    if (authRequired && !loggedIn) {
        return next('/login');
    }

    next();
});
