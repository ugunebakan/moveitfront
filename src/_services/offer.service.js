import { accountService } from '../_services';

export const offerService = {
    getAllOffers,
    getSingleOffer,
    createSingle
};


function createSingle(data) {
    const requestOptions = {
        method: 'POST',
        headers:{ ...accountService.authHeader(), 'Content-type': 'application/json'},
        body: JSON.stringify(data)
    };

    return fetch('/api/offer/', requestOptions).then(handleResponse);
}


function getAllOffers() {
    const requestOptions = {
        method: 'GET',
        headers: accountService.authHeader()
    };

    return fetch('/api/offer/', requestOptions).then(handleResponse);
}

function getSingleOffer(id) {
    const requestOptions = {
        method: 'GET',
        headers: accountService.authHeader()
    };

    return fetch('/api/offer/' + id, requestOptions).then(handleResponse);
}


function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                accountService.logout();
                location.reload(true);
            }
            const error = data || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}
