import Vue from 'vue';
import App from './App.vue';
import { router } from './_routers';
import { store } from './_store';
Vue.config.productionTip = false;

new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
});
